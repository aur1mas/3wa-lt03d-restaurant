<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    protected $fillable =[
    	'title',
    	'photo',
    	'description',
    	'netto_price',
    	'price',
    	'quantity'
    ];
    public function getFormattedPriceAttribute($value)
    {
       return number_format($this->price / 100, 2);
    }
    public function getFormattedNetoPriceAttribute($value)
    {
       return number_format($this->netto_price / 100, 2);
    }
    public function order_lines()
   {
     return $this->hasMany('App\OrderLine');
   }

   public function getWithoutVat()
   {
       return \App\Price::getWithoutVat($this->FormattedPrice);
   }

   public function getVat()
   {
       return $this->FormattedPrice - $this->getWithoutVat();
   }
}
