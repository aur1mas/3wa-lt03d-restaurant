<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Table;

class CheckoutController extends Controller
{
    public function info()
    {
        $tables = Table::all()->pluck('name', 'id');

        return view('checkout.info', compact('tables'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [

           'name' => 'required|max:255',
           'tables' => 'required|exists:tables,id',
           'email' => 'required|email|max:255',
           'reservation_date' => 'nullable|date|date_format:Y-m-d',
           'contact_phone' => 'required|numeric',
           'number_of_persons' => 'required|numeric|between:0,15',
           'reservation_time' => 'required',

        ]);

        $reservationInfo = [
        'table_id' => $request->tables,
        'name' => $request->name
        ];

        session()->put('cart.reservation_info', $reservationInfo);

        return redirect()->route('checkout.confirm');
    }

    public function confirm()
    {
        $table = Table::find(session('cart.reservation_info.table_id'));
        return view('checkout.confirm', compact('table'));
    }
}
