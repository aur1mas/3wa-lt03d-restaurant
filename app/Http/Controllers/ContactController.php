<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct() {
        $this->middleware('auth.admin')->except('show');
    }

    public function show() {
    	$contact = Contact::first();
    	return view('contact.show', compact('contact'));
    }

    public function edit() {
		$contact = Contact::first();
    	return view('contact.form', compact('contact'));
    }

    public function update(Request $request) {
    	$contact = Contact::first();

    	if($contact) {
            // Update existing record
    		$contact->update($request->all());
    	} else {
            // Store new record
    		Contact::create($request->all());
    	}
    	return redirect()->route('contact.show');
    }
}
