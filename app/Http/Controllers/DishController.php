<?php

namespace App\Http\Controllers;

use App\Dish;
use Illuminate\Http\Request;

class DishController extends Controller
{
    public function __construct() {
        $this->middleware('auth.admin')
             ->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dishes = Dish::all();
        return view('dish.index', compact('dishes')); /**['dishes'=> $dishes] tas pats butu compact($dishes)*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('dish.form', compact('dish'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('photo')){
            $file = $request->file('photo');
            $file->store('public');
            $photo= $file->hashName();
            }

       Dish::create([
            'title' => $request->get('title'),
            'photo' => $request->photo,
            'description' => $request->get('description'),
            'price' => $request->get('price'),
            'netto_price' => $request->get('netto_price'),
            'quantity' => $request->get('quantity'),
          ]);


        return redirect()->route('dishes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dish = Dish::find($id);
        return view('dish.show', compact('dish'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function edit(Dish $dish)
    {
        return view('dish.form', compact('dish'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dish $dish)
    {
           if ($request->hasFile('photo')){
            $file = $request->file('photo');
            $file->store('public');
            $request->photo= $file->hashName();
            }

           $dish->title= $request->title;
           $dish->description = $request->description;
           $dish->price = $request->price;
           $dish->netto_price = $request->netto_price;
           $dish->quantity = $request->quantity;
           $dish->photo = $request->photo;

           $dish->update();

        return redirect()->route('dishes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dish  $dish
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dish::find($id)->delete();
        return redirect()->route('dishes.index');

    }
}
