<?php

namespace App\Http\Controllers;

use App\Dish;
use App\Order;
use App\OrderLine;
use App\Table;
use Illuminate\Http\Request;
use App\Price;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.admin')
             ->except(['index', 'store', 'show', 'addToCart', 'clearCart', 'deleteLine', 'checkout']);

        $this->middleware('auth')
             ->except(['addToCart', 'clearCart', 'deleteLine', 'checkout']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::user()->isAdmin()) {
            $orders = Order::orderBy('date', 'desc')->get();
        } else {
            $orders = \Auth::user()->orders()->orderBy('date', 'desc')->get();
        }

       return view('order.index', ['orders'=> $orders]);
    }
//


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       if (session('cart.total')){
        return view('order.form');
       } else {
        return redirect()->route('dishes.index');
       }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = Order::create([
            'name' => session('cart.reservation_info.name'),
            'email' => 'aurimas.baubkus@gmail.com',
            'total' => session('cart.total'),
            'date' => \Carbon\Carbon::now(),
            'user_id' => \Auth::user()->id,
            'reservation_date' => '2017-03-22',
            'reservation_time' => '20:00',
            'number_of_persons' => 2,
            'table_id' => session('cart.reservation_info.table_id')
        ]);

        foreach(session('cart.items') as $item) {
            OrderLine::create([
                'order_id' => $order->id,
                'dish_id' => $item['id'],
                'quantity' => $item['quantity'],
                'total' => $item['total']
            ]);
        }
        $this-> clearCart();
        return redirect()->route('orders.index')->with('message', [
                'text' => 'Užsakymas sėkmingai pateiktas!',
                'type' => 'success'
            ]
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        return view('order.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $dishes = Dish::all()->pluck('title', 'id');
        return view('order.form', compact('order', 'dishes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

           'name' => 'required|max:255',
           'tables' => 'required|exists:tables,id',
           'email' => 'required|email|max:255',
           'reservation_date' => 'nullable|date|date_format:Y-m-d',
           'contact_phone' => 'required|numeric',
           'number_of_persons' => 'required|numeric|between:0,15',
           'reservation_time' => 'required',

        ]);

        Order::find($id)->update($request->all());


        return redirect()->route('orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */


    public function destroy(Order $order, Request $request)
    {
        $order->order_lines()->delete();
        $order->delete();

        if ($request->has('redirect_url')) {
            return redirect()->to($request->redirect_url);
        }

        return redirect()->route('orders.index');
    }

    public function addToCart(Request $request){
        // session(['cart.items'=> [] ]);
        $dish_id = $request->id;

        $dish = Dish::find($dish_id);
        $product = [
            'id' => $dish->id,
            'photo' => $dish->photo,
            'title' => $dish->title,
            'quantity' => 1,
            'price' => $dish->FormattedPrice,
            'total' =>$dish->FormattedPrice,
            'without_vat' => $dish->getWithoutVat(),
            'vat' => $dish->getVat()
        ];
            $items = session('cart.items');
            $existing = false;
            $grand_total = 0;

        if ($items && count($items) > 0) {

            foreach($items as $index => $item) {
                if($item['id'] == $dish_id) {
                    $items[$index]['quantity'] = $item['quantity'] + $product['quantity'];
                    $items[$index]['total'] = $items[$index]['quantity'] * $dish->FormattedPrice;

                    $items[$index]['without_vat'] = $items[$index]['quantity'] * $dish->getWithoutVat();
                    $items[$index]['vat'] = $items[$index]['quantity'] * $dish->getVat();

                    $existing = true;
                    $grand_total = $grand_total + $items[$index]['total'];
                } else{
                    $grand_total = $grand_total + $item['total'];
                }

                }
            }

           if(! $existing){
                session()->push('cart.items', $product);
                $grand_total += $product['total'];
            } else {
                session(['cart.items'=> $items]);
            }

            $withoutVat = Price::getWithoutVat($grand_total);
            $vat = Price::getVat($grand_total);

            session([
                'cart.total_without_vat' => $withoutVat,
                'cart.total_vat' => $vat,
                'cart.total' => $grand_total
            ]);

            return session('cart');
    }

    public function clearCart() {
        session(['cart.items' => [], 'cart.total' =>0]);
    }

    public function deleteLine(Request $request)
    {
        $id = $request->id;
        $items = session('cart.items');

        $total = session('cart.total');

        foreach ($items as $index => $item) {
            if ($item['id'] == $id) {
                $total -= $item['total'];
                unset($items[$index]);
                break;
            }
        }

        session([
            'cart.items' => $items,
            'cart.total' => $total
        ]);

        return redirect()->route('cart.checkout');
    }


    public function checkout()
    {
        return view('order.checkout');
    }

    public function destroyLine($id)
    {
        $line = OrderLine::findOrFail($id);
        $line->delete();

        $order = Order::find($line->order->id);
        $order->recalculate();
        $order->save();

        return redirect()->route('orders.edit', $line->order->id)->with('message', [
            'type' => 'info',
            'text' => 'dish removed'
        ]);
    }

    public function addToOrder($id, Request $request)
    {
        $order = Order::findOrFail($id);
        $collection = OrderLine::where([
            'dish_id' => $request->dish,
            'order_id' => $order->id
        ])->get();

        $dish = Dish::findOrFail($request->dish);

        if ($collection->count() > 0) {
            $line = $collection->first();

        } else {
            $line = new \App\OrderLine();
            $line->order_id = $order->id;
            $line->dish_id = $dish->id;
        }

        $line->quantity += $request->quantity;
        $line->total = $dish->price * $line->quantity;
        $line->save();

        $order->recalculate();
        $order->save();

        return redirect()->route('orders.edit', $order->id)->with('message', [
            'text' => 'Dish added!',
            'type' => 'success'
        ]);
    }
 }
