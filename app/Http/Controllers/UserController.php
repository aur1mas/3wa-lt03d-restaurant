<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth.admin')
             ->except(['profile', 'update']);
    }

    public function index()
    {
        $users = User::all();
        return view('user.index', compact('users'));
    }

    public function profile()
    {
        $user = \Auth::user();
        $title = 'Update profile';
        return view('auth.register', compact('user', 'title'));
    }

    public function update(Request $request)
    {
        $user = \Auth::user();

        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->dob = $request->dob;
        $user->phone = $request->phone;
        $user->city = $request->city;
        $user->zip = $request->zip;
        $user->country = $request->country;
        $user->save();

        return redirect()->route('dishes.index')->with('message', [
            'text' => 'Profile updated!',
            'type' => 'success'
        ]);
    }

    public function show(User $user)
    {
        return view('user.show', compact('user'));
    }
}
