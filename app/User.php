<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname','email', 'password', 'dob', 'phone', 'address', 'city', 'zip',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function orders() {
        return $this->hasMany('App\Order');
    }

    /**
     * išrūšiuoja order'ius pagal kainą nuo didžiausios
     */
    public function getOrdersByPrice()
    {
        return $this->orders()->orderBy('total', 'desc')->get();
    }

    public function isAdmin()
    {
        return $this->type === 'admin';
    }

    public function getCountryName()
    {
        $countries = [
            'LT' => 'Lithuania'
        ];

        return isset($countries[$this->country]) ? $countries[$this->country] : $this->country;
    }
}
