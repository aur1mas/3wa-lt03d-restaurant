<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('table_id')->unsigned()->nullable();
            $table->integer('number_of_persons')->unsigned()->nullable();
            $table->string('contact_phone')->nullable();
            $table->date('reservation_date')->nullable();
            $table->time('reservation_time')->nullable();

            $table->foreign('table_id')->references('id')->on('tables');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('table_id');
            $table->dropColumn('number_of_persons');
            $table->dropColumn('contact_phone');
            $table->dropColumn('reservation_date');
            $table->dropColumn('reservation_time');
        });
    }
}
