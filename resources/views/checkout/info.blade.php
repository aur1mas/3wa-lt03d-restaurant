@extends('layouts.app')

@section('content')

	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

    {!! Form::open(['route' => 'checkout.store', 'method' => 'post']) !!}

    {!! Form::label('name', 'Vardas') !!}
    {!! Form::text('name', Auth::user() ? Auth::user()->name : null, ['class' => 'form-control', 'placeholder' => 'vardas']) !!}

    {!! Form::label('tables', 'Pasirinkite staliuką prie kurio sėdėsite') !!}
    {!! Form::select('tables', $tables, null, ['class' => 'form-control', 'placeholder' => 'Select table']) !!}

    {!! Form::submit('Reserve' ,['class' => 'btn-primary']) !!}

    {!! Form::close() !!}

@endsection
