@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Contact page form</div>
                    <div class="panel-body">

                    @if(isset($contact))
            			{!! Form::model($contact, [
            			'route' => ['contact.update'],
            			'method' => 'put',
                        'class' => 'form-horizontal'
            			]) !!}
            		@else
            			{!! Form::open(['route' => 'contact.update', 'method' => 'put', 'class' => 'form-horizontal']) !!}
            		@endif


                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Address</label>

                                <div class="col-md-6">
                                    {!! Form::text('address', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Email</label>

                                <div class="col-md-6">
                                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Phone</label>

                                <div class="col-md-6">
                                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

							<div class="form-group">
                                <label for="name" class="col-md-4 control-label">Working hours</label>

                                <div class="col-md-6">
                                    {!! Form::textarea('hours', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Map embed code</label>

                                <div class="col-md-6">
                                    {!! Form::textarea('map', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>


                            

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
