@extends('layouts.app')

@section('content')
	@if(isset($contact) && $contact)
	<div class="container">

		<div class="row">
			<div class="col-md-6 text-left">
				<h1>{{ $contact->title }}</h1>
				
				<p>Address: {{ $contact->address }}</p>
				<p>Email: <a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></p>
				<p>Phone: <a href="tel:{{ $contact->phone }}">{{ $contact->phone }}</a></p>
			</div>
			<div class="col-md-6">
				{!! $contact->hours !!}
			</div>
		</div>
	</div>

	{!! $contact->map !!}

	<div class="text-center">
		<a href="{{ route('dishes.index') }}" class="btn btn-primary btn-lg">Book a table</a>
	</div>
	@else
	<div class="alert alert-danger">
		<h2>Sorry, no contact :)</h2>
	</div>
	@endif
@endsection