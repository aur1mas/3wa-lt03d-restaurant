@extends('layouts.app')


	@section('content')
		
		<h2>Tik pabandyk pakeisti...!</h2>

		@if(isset($dish))
			{!! Form::model($dish, [
			'route' => ['dishes.update', $dish->id],
			'method' => 'put',
			'files' => true
			]) !!}
		@else
			{!! Form::open(['route' => 'dishes.store', 'method' => 'post', 'files' => true]) !!}
		@endif
			
			<div class="from-group">    
			{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title'])!!}
			</div>
			
			<div class="from-group">  
			{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
			</div>
			
			
			
						
			<div class="from-group">  
			{!! Form::number('price', null, ['class' => 'form-control', 'placeholder' => 'Price']) !!}
			</div>
			<div class="from-group">  
			{!! Form::number('netto_price', null, ['class' => 'form-control', 'placeholder' => 'Netto Price']) !!}
			</div>
			
			<div class="from-group">  
			{!! Form::number('quantity', null, ['class' => 'form-control', 'placeholder' => 'Quantity']) !!}
			</div>

			<div class="from-group">  
			{!! Form::file('photo', null, ['class' => 'form-control', 'placeholder' => 'IMAGE']) !!}

			</div>
	@if (Auth::check()) 

			{!! Form::submit('Save' ,['class' => 'btn-primary']) !!}
	
		@endif 	

		{!! Form::close() !!}



		@if(isset($dish))
			{!! Form::open(['route' => ['dishes.destroy', $dish->id], 'method' => 'delete'])!!}
				{!! Form::submit('Delete' , ['class' => 'btn btn-danger'])!!}
			{!! Form::close() !!}
		@endif

@endsection