@extends('layouts.app')

@section('content')

<h2>MUSU patiekalai</h2>

@if(Auth::user() && Auth::user()->isAdmin())
<a href="{{route('dishes.create') }}" class="btn btn-primary" role="button">Sukurti nauja</a> 
@endif		

@foreach ($dishes->chunk(4) as $chunk) 
<div class="row">
	@foreach ($chunk as $dish)
	<div class="col-md-3"> {{ $dish->title }}
		<div class="thumbnail">
			<img class="img-responsive" src="{{ $dish->photo }}" >

			<div class="caption">
				<h3> {{ $dish->title }} </h3>
				<p> {{ $dish->description }} </p>
				<p>
					<strong>KAINA:</strong>
					{{ $dish->FormattedPrice }} 
					<strong>KAINA:</strong>
					{{ $dish->FormattedNetoPrice }}
					<strong>EUR</strong>
					<br>
					<strong>KIEKIS:</strong>
					{{ $dish->quantity }} 
					<strong>vnt.</strong>
				</p>
				<p>
					<a href="{{route('dishes.show', $dish->id) }}" class="btn btn-primary" role="button">PERZIURETI</a>
					<a href="#" class="btn btn-warning add-to-cart"  data-id="{{ $dish->id }}" role="button">Add to cart</a>  
				</p>
			</div>			
		</div>
	</div>
	@endforeach	
</div>
@endforeach	
@endsection
