@extends('layouts.app')

	@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<h2>{{ $dish->title }}</h2>
				<div style="background-image: url({{ $dish->photo }}); height:300px; background-position:center;  background-size: cover;"></div>
				
				<p>{{ $dish->description }}</p>
				<ul>
					<li>{{ $dish->Formattedprice }}<strong> EUR</strong></li>
					<li>{{ $dish->quantity }}<strong> vnt.</strong></li>
				</ul>


			@if (Auth::check()) 
	   		 <a href="{{route('dishes.edit', $dish->id) }}" class="btn btn-primary">EDIT</a>
			@endif 

		</div>

	</div>

	</div>

		
	@endsection