@extends('layouts.app')

@section('content')

<div class="table-responsive">
  <table class="table">

	<tbody>
		<thead>
			<th>Pavadinimas</th>
			<th>Kiekis</th>
			<th>Vnt. Kaina</th>
            <th>PVM</th>
            <th>Viso be PVM</th>
			<th>Viso</th>
			<th></th>
		</thead>
	@if(session('cart.items') && count(session('cart.items'))>0)
        @foreach(session('cart.items') as $item)
            <tr>
                <td>{{ $item['title']}} <img src="{{$item['photo']}}" alt="{{ $item['title']}}" class="img-rounded" style="width: 70px;"> </td>
                <td>{{ $item['quantity']}} vnt.</td>
                <td>{{ $item['price']}}€</td>
                <td>{{ $item['vat'] }}€</td>
                <td>{{ $item['without_vat'] }}€</td>
                <td>{{ $item['total']}} &euro;</td>
               <td><a class="clear-line-cart" href="{{ route('cart.delete_line', ['id' => $item['id']] ) }}"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
             </tr>

            @endforeach
            <tfoot>
            	<td></td>
            	<td></td>
            	<td>Viso: </td>
                <td>{{ session('cart.total_vat') ?: '0' }} €</td>
            	<td>{{ session('cart.total_without_vat') ?: '0' }} &euro;</td>
            	<td> {{session('cart.total') ?: '0'}} &euro;</td>
            </tfoot>
        @endif

	</tbody>

  </table>

   <a href="{{route('checkout.info') }}" class="btn btn-success">Continue</a>
</div>







@endsection
