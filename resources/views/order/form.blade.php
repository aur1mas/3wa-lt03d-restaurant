@extends('layouts.app')


	@section('content')

		<h2>Iveskite uzsakyma rankutemis, jei laukai yra tusti...!</h2>

		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		@if(isset($order))
			{!! Form::model($order, [
			'route' => ['orders.update', $order->id], 'method' => 'put'
			]) !!}
		@else
			{!! Form::open(['route' => 'orders.store', 'method' => 'post']) !!}
		@endif

			<div class="from-group">
			{!! Form::text('name', Auth::user() ? Auth::user()->name: null, ['class' => 'form-control', 'placeholder' => 'Name'])!!}
			</div>
			<div class="from-group">
			{!! Form::text('email', Auth::user() ? Auth::user()->email: null, ['class' => 'form-control', 'placeholder' => 'Email'])!!}
			</div>
		@if(session('cart.total'))
		<p> Suma: {{session('cart.total') }} &euro;</p>
		@else
			<div class="from-group">
			{!! Form::number('total', null, ['class' => 'form-control', 'placeholder' => 'Total']) !!}
			</div>
		@endif

		@if (Auth::check())
			{!! Form::submit('Save' ,['class' => 'btn-primary']) !!}
			{!! Form::close() !!}
		@endif

		@if (count($order->order_lines))
			<table class="table">
				<tr>
					<th>Dish</th>
					<th>Quantity</th>
					<th>Price</th>
					<th></th>
				</tr>
			@foreach ($order->order_lines as $line)
				<tr>
					<td>{{ $line->dish->title }}</td>
					<td>{{ $line->quantity }}</td>
					<td>{{ $line->total }} &euro;</td>
					<td>
						{!! Form::open(['route' => ['orders.line_destroy', $line->id], 'method' => 'delete'])!!}
							{!! Form::submit('Delete' , ['class' => 'btn btn-danger'])!!}
						{!! Form::close() !!}
					</td>
				</tr>
			@endforeach
			</table>
		@endif

		{!! Form::open(['route' => ['order.add_dish', $order->id], 'method' => 'put']) !!}
			{!! Form::number('quantity', null, ['class' => 'form-control', 'placeholder' => 'Quantity']) !!}
			{!! Form::select('dish', $dishes, ['class' => 'form-control', 'placeholder' => 'Select dish']) !!}
			{!! Form::submit('Add Dish', ['class' => 'btn btn-primary']) !!}
		{!! Form::close() !!}

		@if(isset($order))
			{!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete'])!!}
				{!! Form::submit('Delete' , ['class' => 'btn btn-danger'])!!}
			{!! Form::close() !!}
		@endif

@endsection
