@extends('layouts.app')

@section('content')

<h2>Užsakymų sąrašas:</h2>

		<hr>
			<div class="row">
				@foreach ($orders as $order)
					<div class="col-md-6 col-md-offset-3">
						<ul>
							<li>
								<h4>Vardas: {{ $order->name }} </h4>
								<p>el.paštas: {{ $order->email }} </p>
								<p>
									PVM {{ $order->getVat() }} € <br />
									Suma be PVM: {{ $order->getWithoutVat() }} €<br />
									Užsakymo suma:{{ $order->total }} €
								</p>
								<p>Užsakymo data: {{ $order->date }}
							<p><a href="{{route('orders.show', $order->id) }}" class="btn btn-primary" role="button">Perziurėti</a></p>

							</li>
							<hr>
						</ul>



					</div>
		 		@endforeach

			</div>
 		<hr>
 		<a href="{{route('orders.create') }}" class="btn btn-primary" role="button">Sukurti nauja</a>
@endsection
