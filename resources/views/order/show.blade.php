@extends('layouts.app')

	@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<ul>
					<li>{{ $order->name }} </li>
					<li>{{ $order->email }} </li>
					<li>{{ $order->total }} </li>
					<li>{{ $order->date }} </li>
					@if (Auth::check()) 
	   		 <a href="{{route('orders.edit', $order->id) }}" class="btn btn-primary">Redaguoti</a>
			@endif 
				</ul>
				<table class="table">
						<thead>
							<th>Pavadinimas</th>
							<th>Kiekis</th>
							<th>Kaina</th>
							<th>Viso</th>
							<th>Siuksline</th>
						</thead>
					@if(count($order->order_lines()) >0)
				       <tbody>
				        @foreach($order->order_lines as $item) 
				            <tr>         
				                <td>{{ $item->dish->title }} <img src="{{$item->dish->photo}}" alt="{{ $item->dish->title }}" class="img-rounded" style="width: 70px;"> </td>
				                <td>{{ $item->quantity }} </td>
				                <td>{{ $item->dish->price }} </td>
				                <td>{{ $item->total }} </td>
				                <td><a id="clear-line-cart" href="#" ><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
				            </tr>  
				        @endforeach
				        </tbody>
				        <tfoot>
			            	<tr>
			            		<td colspan="3" class="text-right">Total:</td>
			            		<td>{{ $order->total }}</td>
			            	</tr>
				        </tfoot>  
				    @endif
				</table>
   				<a href="{{route('orders.create') }}" class="btn btn-success">PIRKTI</a>
			</div>
		</div>
	</div>
	@endsection

