<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('justas', 'HomeController@justas');

Route::get('/', 'DishController@index');

Auth::routes();

Route::resource('dishes', 'DishController');

Route::resource('orders', 'OrderController');
Route::delete('orders/line/{id}/destroy', 'OrderController@destroyLine')->name('orders.line_destroy');
Route::put('order/{id}/add-dish', 'OrderController@addToOrder')->name('order.add_dish');

Route::resource('tables', 'TableController');
Route::resource('users',  'UserController');

//cart route
Route::post('cart', 'OrderController@addToCart')->name('cart.add');
Route::delete('cart', 'OrderController@clearCart')->name('cart.clear');
Route::get('cart', 'OrderController@checkout')->name('cart.checkout');
Route::get('cart/delete/{id}', 'OrderController@deleteLine')->name('cart.delete_line');
Route::get('cart/reservation', 'CheckoutController@info')->name('checkout.info');
Route::post('cart/reservation', 'CheckoutController@store')->name('checkout.store');
Route::get('cart/confirm', 'CheckoutController@confirm')->name('checkout.confirm');

// user routes
Route::get('/profile', 'UserController@profile')->name('profile')->middleware('auth');
Route::put('/profile', 'UserController@update')->name('profile.update')->middleware('auth');

// contact page
Route::get('contact', 'ContactController@show')->name('contact.show');
Route::get('contact/edit', 'ContactController@edit')->name('contact.edit');
Route::put('contact', 'ContactController@update')->name('contact.update');
